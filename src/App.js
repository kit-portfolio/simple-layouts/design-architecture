import React from 'react';
import "./theme/common.scss";
import Navbar from "./sections/navbar/navbar";
import Cover from "./sections/cover/cover";
import PopularProjects from "./sections/popular-projects/popular-projects";
import PopularClients from "./sections/popular-clients/popular-clients";
import TopRated from "./sections/top-rated/top-rated";
import HotNews from "./sections/hot-news/hot-news";
import Footer from "./sections/footer/footer";

export default function App() {
    return (
        <div className="app">
            <Navbar/>
            <Cover/>
            <PopularProjects/>
            <PopularClients/>
            <TopRated/>
            <HotNews/>
            <Footer/>
        </div>
    );
}


// TODO: fix bug in popular posts
// TODO: parallax
// TODO: search
// TODO: fix links