import React from "react";
import {Link} from "react-router-dom";
import './project.scss';

export default function Project(props) {
    const {title, link, cover} = props.content;

    return (
        <Link to={link} className="popular-project">
            <img
                src={cover}
                alt={title}
                className="popular-project-cover"
            />
            <p className="popular-project-title">{title}</p>
        </Link>
    );
}