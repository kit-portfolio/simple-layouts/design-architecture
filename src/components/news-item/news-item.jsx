import React from "react";
import {Link} from 'react-router-dom';
import ResizeObserver from 'react-resize-observer';
import './news-item.scss';

export default function NewsItem(props) {
    const {title, desc, cover, href, id} = props.content;
    let index = props.index;

    return (
        <Link to={href} id={`news-${id}`} style={{gridArea: `item-${++index}`}} className="news-item-container">
            <ResizeObserver
                onResize={(rect) => {
                    const el = document.getElementById(`news-${id}`);
                    if (rect.width === 270) el.classList.add("news-item-layout-s");
                    if (rect.width === 580) el.classList.add("news-item-layout-m");
                    if (rect.width === 890) el.classList.add("news-item-layout-l");
                }}
            />
            <div className="news-item-cover" style={{backgroundImage: `url(${window.location.href + cover})`}} />
            <div className="news-item-info">
                <p className="news-item-header">{title}</p>
                <p className="news-item-description">{desc}</p>
            </div>
        </Link>
    );
}