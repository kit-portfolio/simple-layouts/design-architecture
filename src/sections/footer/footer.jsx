import React from "react";
import './footer.scss';
import {ReactComponent as LioitIcon} from "../../components/pictograms/lioit.svg";

export default function Footer() {

    return (
        <section className="section-footer">
            <div className="footer-container">
                <p className="footer-text"> All Rights Reserved © 2014 architect.com - Interior Design & Architecture
                    Magazine</p>
                <LioitIcon className="footer-logo-image"/>
                <p className="footer-logo-text">lioit</p>
            </div>
        </section>
    );
}