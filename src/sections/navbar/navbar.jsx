import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import './navbar.scss';
import {ReactComponent as Logo} from "../../components/pictograms/logo.svg";
import {ReactComponent as SearchIcon} from "../../components/pictograms/search.svg";

export default function Navbar() {
    const [navbarItems, setNavbarItems] = useState([]);

    useEffect(() => {
        fetch('mocks/navbar-items.json')
            .then(res => res.json())
            .then(data => setNavbarItems(data.data))
    }, []);

    function renderNavbarItems(navbarItems) {
        return navbarItems.map((item, index) => <Link to={item.href} className="navbar-menu-item"
                                                      key={index}>{item.title}</Link>)
    }

    return (
        <section className="navbar-section">
            <div className="navbar">
                <Link to="#" className="navbar-logo-container">
                    <Logo className="navbar-logo-icon"/>
                    <p className="navbar-logo-text">Architect</p>
                </Link>
                <div className="navbar-items-container">
                    {renderNavbarItems(navbarItems)}
                </div>
                <div className="search-container">
                    <SearchIcon className="navbar-search-icon"/>
                </div>
            </div>
        </section>
    );
}