import React, {useEffect, useState} from "react";
import './popular-projects.scss';
import Project from "../../components/project/project";

export default function PopularProjects() {
    const [popularPosts, setPopularPosts] = useState([]);

    useEffect(() => {
        fetch('mocks/popular-projects.json')
            .then(res => res.json())
            .then(data => setPopularPosts(data.data))
    }, []);

    return (
        <section className="popular-projects-section">
            <h2 className="popular-projects-header">Most popular projects</h2>
            <div className="popular-projects-container">
                {popularPosts.map((item, index) => <Project content={item} key={index}/>)}
            </div>
        </section>
    );
}