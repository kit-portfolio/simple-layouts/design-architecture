import React, {useEffect, useState} from 'react';
import './popular-clients.scss';

export default function PopularClients() {
    const [popularClients, setPopularClients] = useState([]);

    useEffect(() => {
        fetch('mocks/popular-clients.json')
            .then(res => res.json())
            .then(data => setPopularClients(data.data))
    }, []);

    function renderPopularClients(popularClients) {
        return popularClients.map((item, index) => <img
            src={item.logo}
            alt={item.title}
            key={index}
        />)


    }

    return (
        <section className="popular-clients">
            <h2 className="popular-clients-header">our most popular clients</h2>
            <div className="popular-clients-divider"/>
            <div className="popular-clients-logo-container">
                {renderPopularClients(popularClients)}
            </div>
        </section>
    );
}