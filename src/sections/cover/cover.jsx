import React from "react";
import {Link} from "react-router-dom";
import './cover.scss';

export default function Cover() {

    return (
        <section className="cover-section">
            <h1 className="cover-header">the latest news on design & architecture</h1>
            <Link to="#" className="cover-button subscribe-button">Subscribe Now</Link>
            <Link to="#" className="cover-button articles-button">Best Articles</Link>
        </section>
    );
}