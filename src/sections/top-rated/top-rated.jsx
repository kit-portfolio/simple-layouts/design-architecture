import React, {useEffect, useState} from "react";
import './top-rated.scss';

export default function TopRated() {
    const [topRated, setTopRated] = useState([]);

    useEffect(() => {
        fetch('mocks/top-rated.json')
            .then(res => res.json())
            .then(data => setTopRated(data.data))
    }, []);

    function renderTopRated(topRated) {
        return topRated.map((item, index) => <div
            className="top-rated-image-container"
            key={index}
        >
            <img
                src={`mocks/img/top-rated/${item}.png`}
                className="top-rated-image"
                alt={item}
                key={index}
            />
        </div>)
    }

    return (
        <section className="section-top-rated">
            <h2 className="top-rated-header">top rated</h2>
            <div className="top-rated-pics">
                {renderTopRated(topRated)}
            </div>
        </section>
    );
}