import React, {useEffect, useState} from 'react';
import './hot-news.scss';
import NewsItem from "../../components/news-item/news-item";

export default function HotNews() {
    const [hotNews, setHotNews] = useState([]);

    useEffect(() => {
        fetch('mocks/hot-news.json')
            .then(res => res.json())
            .then(data => setHotNews(data.data))
    }, []);

    return (
        <section className="section-hot-news">
            <h2 className="hot-news-header">hot-news</h2>
            <div className="hot-news-container">
                <div className="hot-news-container">
                    {hotNews.map((item, index) => <NewsItem content={item} key={index} index={index}/>)}
                </div>
            </div>
        </section>
    );
}