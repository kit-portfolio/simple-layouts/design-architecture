# Design Architecture

### Tech stack
* React
* SaSS

### Demo
Live demo is hosted [here](https://kit-portfolio.gitlab.io/simple-layouts/design-architecture/).

### Launch
To run the project follow the next steps:
* Install node modules
* Run `yarn start` or `npm run start`
* Enjoy

# Preview
![Project preview](preview.png#center)